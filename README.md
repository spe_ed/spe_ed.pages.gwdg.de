<!--
*** Build using the Best-README-Template.
-->

<!-- PROJECT LOGO -->
<br />
<p align="center">
![SPE_ED](spe_ed_logo_side.png "SPE_ED Website")
  <h3 align="center">SPE_ED Website</h3>

  <p align="center">
    This is a project developed for the 2021 InformatiCup
    <br />
    <br />
    <a href="https://gitlab.gwdg.de/spe_ed/spe_ed.pages.gwdg.de/issues">Report Bug</a>
    ·
    <a href="https://gitlab.gwdg.de/spe_ed/spe_ed.pages.gwdg.de/issues">Request Feature</a>
  </p>
</p>

Website: https://spe_ed.pages.gwdg.de
