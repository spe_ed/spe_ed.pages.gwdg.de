# Webserver

### [Repository](https://gitlab.gwdg.de/spe_ed/spe_ed_webserver)

Spe_ed_webserver ist eine Implementierung einer Websocket API für das Spiel spe_ed. Dazu verwendet es die [python Implementation von spe_ed](https://gitlab.gwdg.de/spe_ed/spe_ed_game).

Spe_ed_webserver kann als drop-in Ersatz für die Websocket API [wss://msoll.de/spe_ed](wss://msoll.de/spe_ed) von [https://phinau.de/spe_ed/](https://phinau.de/spe_ed/) genutzt werden.
