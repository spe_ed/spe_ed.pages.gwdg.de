# PPO

### [Repository](https://gitlab.gwdg.de/spe_ed/spe_ed_ppo)

## Über den Ansatz
PPO (_[Proximal Policy Optimization](https://openai.com/blog/openai-baselines-ppo/)_) ist ein von OpenAI entwickelter Reinforcement-learning Algorithmus, welcher mit der Motivation entwickelt wurde, den Optimierungsprozess [effizienter zu gestalten](https://spinningup.openai.com/en/latest/algorithms/ppo.html).

## Über die Implementation
Für die Implementation von PPO wird in diesem Repository OpenAI Gym verwendet.
Gym ist ein von OpenAI entwickeltes Toolkit zur Entwicklung von Umgebungen für Reinforcement-Learning Algorithmen.
