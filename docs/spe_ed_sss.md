# State Space Search

### [Repository](https://gitlab.gwdg.de/spe_ed/spe_ed_statespacesearch_kotlin)

## Über den Ansatz
[State Space Search](https://en.wikipedia.org/wiki/State_space_search) ist ein Suchalgorithmus über den Raum der erreichbaren Zustände, der eine Möglichkeit bietet, in einer deterministischen Umgebung mit finiter Aktionsmenge eine Reihenfolge an Aktionen zu suchen, die zu einem vorteilhaften Zustand führen. Der erreichbare State Space lässt sich als Baum verdeutlichen, bei dem die verschiedenen Zustände die Knoten und die verschiedenen Aktionen die Kanten darstellen.

## Über die Implementation
Wir benutzen eine abgewandelte Form der State Space Search, da wir nicht einen einzelnen Zustand suchen, zu dem wir uns dann sicher hinbewegen können. Wir versuchen eher den State Space zu erkunden, vollständig oder durch eine heuristische Funktion, um dann gröbere Einschätzungen auf den 5 Teilbäumen, die im State Space durch die erste Aktion geformt werden, zu machen.

Der Spieler nutzt Breitensuche um den State Space zu erkunden, einen Spieler für Tiefensuche, und einen Spieler, der mithilfe der Bewertungsfunktion besserbewertete States bei der Suche bevorzugt, sind aber auch vorhanden.
Der Spieler verwendet die durchschnittliche Bewertung aller Zustände innerhalb dem der Aktion zugehörigen Teilbaum des Suchbaumes, um eine Aktion zu bewerten. 
Zur Bewertung eines Spielzustands wird eine Connected Component Analyse des Spielfeldes umliegend der Spielerposition benutzt, um die Anzahl der  freien Felder zu bestimmen, die im selben Bereich wie der Spieler selbst liegen.
