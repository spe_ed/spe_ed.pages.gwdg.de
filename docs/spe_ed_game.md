# Game

### [Repository](https://gitlab.gwdg.de/spe_ed/spe_ed_game)

Spe_ed_game ist eine Implementation des Spiels spe_ed in Python entsprechend der durch die [Aufgabestellung](https://github.com/InformatiCup/InformatiCup2021/blob/master/spe_ed.pdf) definierten Regeln

Um die Entwicklung unserer Algorithmen zu vereinfachen, nutzten wir eine zufällige Farbgebung und eine sekundäre Linie auf den gefahrenen Spielerstrecken, um die Wege besser nachvollziehen zu können.

![Game](game.png "Spielfeld")
