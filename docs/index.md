# Projekt

![SPE_ED](spe_ed_logo_side.png "SPE_ED Automat")

### [Repository Group](https://gitlab.gwdg.de/spe_ed/)
Unser Beitrag zum [informatiCup 2021 - spe_ed](https://github.com/informatiCup/informatiCup2021).

> Der informatiCup 2021 widmet sich der Aufgabe, das Spiel spe_ed zu spielen.

Hier findest du die Dokumentation, Infos und Links zu allen Repositories, die wir für den Informaticup 2021 entwickelt haben. 

## Team

Wir sind das 4er Team _underscore von der Georg-August-Universität Göttingen.

## Technologien

In diesen Projekten sind viele Technologien zu entdecken, unter anderem diese:

<div style="display: -ms-flexbox;     display: -webkit-flex;     display: flex;     -webkit-flex-direction: row;     -ms-flex-direction: row;     flex-direction: row;     -webkit-flex-wrap: wrap;     -ms-flex-wrap: wrap;     flex-wrap: wrap;     -webkit-justify-content: space-around;     -ms-flex-pack: distribute;     justify-content: space-around;     -webkit-align-content: stretch;     -ms-flex-line-pack: stretch;     align-content: stretch;     -webkit-align-items: flex-start;     -ms-flex-align: start;     align-items: flex-start;">
<a href="https://www.python.org/"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/64px-Python-logo-notext.svg.png" alt="Python" width="64" height="64" title="Python"></a>
<a href="https://kotlinlang.org/"><img src="logo.svg" alt="Kotlin" width="64" height="64" title="Kotlin"></a>
<a href="https://gradle.org/"><img src="https://gradle.org/images/gradle-knowledge-graph-logo.png" alt="Gradle" width="64" height="64" title="Gradle"></a>
<a href="https://www.docker.com/"><img src="https://www.docker.com/sites/default/files/d8/2019-07/vertical-logo-monochromatic.png" alt="Docker" width="64" height="64" title="Docker"></a>
<a href="https://gym.openai.com/"><img src="https://gym.openai.com/assets/dist/home/footer/home-cta-d0fb5e0574.svg" alt="OpenAi Gym" width="64" height="64" title="OpenAi Gym"></a>
<a href="https://stable-baselines.readthedocs.io/en/master/index.html"><img src="https://stable-baselines.readthedocs.io/en/master/_static/logo.png" alt="stable-baselines" width="64" height="64" title="stable-baselines"></a>
</div>

