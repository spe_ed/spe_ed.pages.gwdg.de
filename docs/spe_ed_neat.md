# NEAT

### [Repository](https://gitlab.gwdg.de/spe_ed/spe_ed_neat)

## Über den Ansatz
NEAT (_Neuro Evolution of augmenting topologies_) ist ein genetischer Algorithmus, welcher künstliche neuronale Netzwerke evolutionär optimiert. Da die Topologie nicht fest vorgegeben ist, sondern in einem iterativen Optimierungsprozess angepasst wird, ergibt sich eine hohe Flexibilität des Netzwerks.

Der NEAT Algorithmus basiert auf dem Paper _Evolving Neural Networks through
Augmenting Topologies_ von Kenneth O. Stanley und Risto Miikkulainen aus dem Jahre 2002.

## Über die Implementation
Im Repository speed_neat wird dieser Ansatz mit der python library NEAT-Python implementiert.

#### 🏎️ NEAT for Spe_ed  🏎️
