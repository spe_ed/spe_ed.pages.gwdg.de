# Chat

### [Repository](https://gitlab.gwdg.de/spe_ed/spe_ed_chat)

Wir fanden es sehr schade, dass es keinen Chat bei den Onlinespielen gab, in dem man sich mit seinen Mitspielern austauschen konnte. Auch wenn so etwas ohne Moderation nur schwer als Gastgeber des Spiels zu verantworten ist, wollten wir doch gerne einige nette Worte an unsere Mitstreiter verlieren.

Daher implementierten wir den Spe\_ed Chat: Eine Möglichkeit direkt auf dem Spielbrett zu schreiben.

Unser Chat kann pro Spiel einen Text schreiben, solange er dafür genug Platz hat und nicht von den unhöflichen echten Spielern gestört wird.

Er unterstützt A-Z, ist aber in einem switch-case einfach erweiterbar.

![Font](20210116-154114.jpg "Font")

Einfach die MESSAGE env-Variable setzen und schon stehen einem alle Kommunikationsfreiheiten offen, die ein einzelnes kurzes Wort so zu bieten hat.
